def sumandmultarray(array)
    soma = 0
    multi = 0
    array.each do |v| #Para percorrer por cada array interno
        for i in v  #Para percorrer por cada elemento de cada array mais interno
            soma += i  #Soma acumulada de todos os elementos de cada array
            multi *= i  #Multiplicação acumulada de todos os elementos de cada array
        end
    end
    print "O resultado da soma é #{soma} e o resultado da multiplicação é #{multi}" #Escrever na tela o resultado
end
    
#TESTE DA FUNÇÃO 01
vetor = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]];
resul = sumandmultarray(vetor)

def div(num1, num2)
    resuldiv = (num1.to_f/num2.to_f)  #Para transformar cada parâmetro passado para float e depois realizar a divisão.
    puts (resuldiv)
end


#TESTE DA FUNÇÃO 02
n1 = 5
n2 = 2
resul = div(n1, n2)

def quadrado (hashdevalores)
    aux = []  #Array auxiliar para receber o quadrado de cada valor do hash
    hashdevalores.each do |valores|  #Para percorrer o valor de cada chave
        aux.append(valores[1]**2)   #Aqui estou colocando o quadrado de cada valor no final do array auxiliar
    end
    print aux   #Escrever na tela 
end


#TESTE DA FUNÇÃO 03
vetor = {:chave1 => 5, :chave2 => 30, :chave3 => 20}
resul = quadrado(vetor)


def divisiveisportres (array)
    array.each do |valor|  #Percorrer cada elemento do array
       valor = valor.to_s  #Faz um cast em cada elemento do array para transformá-lo em string
    end
    print array  #Escreve na tela o array
end

#TESTE DA FUNÇÃO 04
vetor = [25, 35, 45]
resul = divisiveisportres(vetor)


def divisiveisportres (array)
    aux = []
    array.each do |valor|  #Para percorrer cada valor do array
        if valor % 3 == 0  #Verifica se valor é divisível por 3
            aux.append(valor)  #Adiciona valor no array auxiliar
        end
    end
    print aux  #Para escrever na tela em formato de array
end

#TESTE DA FUNÇÃO 05
vetor = [3, 6, 7, 8]
resul = divisiveisportres(vetor)
